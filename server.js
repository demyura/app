var express = require("express");
var bodyParser = require("body-parser");
var fileUpload = require("express-fileupload");
var path = require("path");
var fs = require("fs");

var app = express();

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());
app.use(fileUpload());

app.get("/api/images", function(req, res) {
  var data = fs.readFileSync("images.json", "utf8");
  var images = JSON.parse(data);
  res.send(images);
})

app.delete("/api/images/:id", function(req, res) {
  var id = req.params.id;
  var data = fs.readFileSync("images.json", "utf8");
  var images = JSON.parse(data);
  var index = -1;

  for (var i = 0; i < images.length; i++) {
    if (images[i].id == id) {
      index = i;
      break;
    }
  }
  if (index > -1) {
    var image = images.splice(index, 1)[0];
    var data = JSON.stringify(images);
    fs.writeFileSync("images.json", data);
    res.send(image);
  } else {
    res.status(404).send();
  }
})

app.post("/api", function(req, res) {
  var data = fs.readFileSync("images.json", "utf8");
  var imageList = JSON.parse(data);
  var id = Math.max.apply(Math, imageList.map(function(o) {
    return o.id;
  }));
  if (id == -Infinity) {
    id = 1;
  } else {
    id++;
  }

  if (!req.files) {
    res.send('no files upload');
  } else {
    var files = [].concat(req.files.files);
    var file;
    var image;
    var images = [];
    for (let i = 0; i < files.length; i++) {
      file = files[i];
      var extention = path.extname(file.name);
      if (extention !== ".jpg") {
        res.send('only ".jpg" files are allowed')
      } else {
        image = {
          id: id,
          name: file.name
        };
        id ++;
        images.push(image);
        file.mv(__dirname + "/temp/img/" + file.name, function(err) {
          if (err) {
            res.status(500).send(err);
          }
        })
      }
    }
    res.send(images);
    var data = fs.readFileSync("images.json", "utf8");
    var imageList = JSON.parse(data);
    imageList = imageList.concat(images);
    var data = JSON.stringify(imageList);
    fs.writeFileSync("images.json", data);
  }
})

app.listen(3003)