var config = require('./config.json');

//========== commander ==========

var program = require('commander');
var env = 'development';

program
  .option('--development', 'development')
  .option('--production', 'production')
  .parse(process.argv);

if (program.development) env = 'development';
if (program.production) env = 'production';

console.log(env);

//========== app-styles ==========

var gulp = require('gulp');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var gulpif = require('gulp-if');

gulp.task('app-styles', function() {
  return gulp.src(config[env].appStyles.sources)
    .pipe(gulpif(config[env].appStyles.sass, sass().on('error', sass.logError)))
    .pipe(gulpif(config[env].appStyles.cssmin, cssmin()))
    .pipe(gulpif(config[env].appStyles.concat, concat(config[env].appStyles.concatName)))
    .pipe(gulp.dest(config[env].appStyles.dest))
    .pipe(connect.reload());
});

//========== app-scripts ==========

var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('app-scripts', function() {
  return browserify(config[env].appScripts.entry)
    .transform('babelify', {
      presets: ["es2015"]
    })
    .bundle()
    .pipe(source(config[env].appScripts.bundleName))
    .pipe(buffer())
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config[env].appScripts.dest))
    .pipe(connect.reload());
});

//========== vendor-styles ==========

gulp.task('vendor-styles', function() {
  return gulp.src(config[env].vendorStyles.sources)
    .pipe(gulpif(config[env].vendorStyles.cssmin, cssmin()))
    .pipe(gulpif(config[env].vendorStyles.concat, concat(config[env].vendorStyles.concatName)))
    .pipe(gulp.dest(config[env].vendorStyles.dest))
});

//========== vendor-scripts ==========

gulp.task('vendor-scripts', function() {
  return gulp.src(config[env].vendorScripts.sources)
    .pipe(gulpif(config[env].vendorStyles.uglify, uglify()))
    .pipe(gulpif(config[env].vendorScripts.concat, concat(config[env].vendorScripts.concatName)))
    .pipe(gulp.dest(config[env].vendorScripts.dest))
});

//========== index ==========

var inject = require('gulp-inject');

gulp.task('index', function() {
  return gulp.src(config[env].index.sources)
    .pipe(inject(gulp.src(config[env].index.injectSrcWithStartTag.source, {
      read: false
    }), {
      starttag: config[env].index.injectSrcWithStartTag.options.starttag,
      ignorePath: config[env].index.injectSrcWithStartTag.options.ignorePath,
      addRootSlash: config[env].index.injectSrcWithStartTag.options.addRootSlash
    }))
    .pipe(inject(gulp.src(config[env].index.injectSrc.source, {
      read: false
    }), {
      ignorePath: config[env].index.injectSrc.options.ignorePath,
      addRootSlash: config[env].index.injectSrc.options.addRootSlash
    }))
    .pipe(gulp.dest(config[env].index.dest))
    .pipe(connect.reload());
});

//========== clean ==========

var del = require('del');

gulp.task('clean', function() {
  return del(config[env].clean.del);
});

//========== build ==========


var runSequence = require('run-sequence');
var fs = require('fs');

gulp.task('build', function(callback) {
  runSequence('clean', ['app-scripts', 'app-styles', 'vendor-styles', 'vendor-scripts'],
    'index',
    callback);
});

//========== replace ==========

gulp.task('replace', function() {
  return gulp.src(['src/img/**'])
    .pipe(gulp.dest(config[env].replace.dest));
})

//========== watch ==========

gulp.task('watch', function() {
  gulp.watch('src/js/*.js', ['app-scripts']);
  gulp.watch('src/styles/*.scss', ['app-styles']);
  gulp.watch('src/index.html', ['index']);
});

//========== default ==========

gulp.task('default', function() {
  runSequence('build', 'replace', ['watch', 'connect']);
  require('./server.js');
});

//========== connect ==========

var connect = require('gulp-connect');
var livereload = require('gulp-livereload');
var proxy = require('http-proxy-middleware');

gulp.task('connect', function() {
  return connect.server({
    root: config[env].connect.root,
    port: config[env].connect.port,
    livereload: true,
    middleware: function(connect, opt) {
      return [
        proxy(config[env].connect.middleware.proxyOptions.path, {
          target: config[env].connect.middleware.proxyOptions.target,
          changeOrigin: true,
          ws: true
        })
      ]
    }
  });
});