import {
    elements,
    tokenList
} from './constants';
import {
    alertAsync
} from './ajax.js'
var activeIndex = 0;

export default function handler(token) {
    elements[tokenList[activeIndex]].menu.removeClass('active');
    elements[tokenList[activeIndex]].content.addClass('hide');
    activeIndex = token;
    elements[tokenList[activeIndex]].menu.addClass('active');
    elements[tokenList[activeIndex]].content.removeClass('hide');
    alertAsync(elements[tokenList[activeIndex]].addres, success);
};


let success = (data) => {
    let divAlert = document.createElement('div');
    divAlert.className = "alert alert-success alert-dismissable";
    divAlert.innerHTML = '<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>' + data;
    $(elements[tokenList[activeIndex]].content).append(divAlert);
    setTimeout(() => {
        $(divAlert).remove()
    }, 3000);
}







