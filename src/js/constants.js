var elements = {
    "home": {
        menu: $('#home'),
        content: $('#homeContent'),
        addres: '/api/home'
    },
    "search": {
        menu: $('#search'),
        content: $('#searchContent'),
        addres: '/api/search'
    },
    "about": {
        menu: $('#about'),
        content: $('#aboutContent'),
        addres: '/api/about'
    }
};


var tokenList = ['home', 'search', 'about'];

export {
    elements,
    tokenList
};