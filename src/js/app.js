import onMenuItemClick from './handler';
import {
  elements,
  tokenList
} from './constants';

for (let i = 0; i < tokenList.length; i++) {
  elements[tokenList[i]].menu.click(() => onMenuItemClick(i))
};

///////////////////// UPLOAD

(function() {
  var dropzone = document.getElementById('dropzone');
  var uploads = document.getElementById("masonryContainer");


  function displayImage(data) {
    for (var i = 0; i < data.length; i++) {
      var div = document.createElement('div');
      div.className = "col-lg-3 col-md-4 col-sm-6 col-xs-12 item";
      div.id = data[i].id;
      div.innerHTML = `
          <div class="item__content">
          <img src="img/${data[i].name}" id="${data[i].id}" alt="">
          <div class="cover-item-gallery">
          <div class="remove-button"><img class="remove-button__img" src="img/trash-icon.png" alt=""></div>
            <a href="">
            </a>
          </div>
          </div>`;
      uploads.appendChild(div);
    }
  }

  function removeImage(img) {
    if (img.className == 'remove-button__img') {
      var elem = img.closest('.item');
      var id = elem.getAttribute('id');
      $(".masonry-container").masonry().masonry('remove', elem);
      $(".masonry-container").masonry().masonry('reloadItems');

      var formData = new FormData(),
        xhr = new XMLHttpRequest();

      xhr.onload = function() {
        var data = JSON.parse(this.responseText);
        //console.log(data);
      }
      xhr.open('delete', '/api/images/' + id);
      xhr.send();
    }
  }

  function addImages(files) {
    var formData = new FormData(),
      xhr = new XMLHttpRequest();

    for (var i = 0; i < files.length; i++) {
      formData.append('files', files[i]);
    }

    xhr.onload = function() {
      //console.log(this.responseText);
      var data = JSON.parse(this.responseText);
      displayImage(data);
      updateMasonry();
      $(".masonry-container").masonry().masonry('reloadItems');
    }
    xhr.open('post', '/api');
    xhr.send(formData);
  }

  function getImages() {
    var xhr = new XMLHttpRequest();
    xhr.open('get', '/api/images');
    xhr.send();
    xhr.onload = function() {
      var data = JSON.parse(this.responseText);
      displayImage(data);
      updateMasonry();
    }
  }

  dropzone.ondrop = function(e) {
    e.preventDefault();
    this.className = "dropzone";
    addImages(e.dataTransfer.files);
  }

  dropzone.ondragover = function() {
    this.className = "dropzone dragover";
    return false;
  }

  dropzone.ondragleave = function() {
    this.className = "dropzone";
    return false;
  }

  document.onclick = function(e) {
    var target = e.target;
    removeImage(target);
    showGallery(target);
  }

  function showGallery(target) {
    if (target.className == 'cover-item-gallery') {
    var elem = target.closest('.item');

    var test = document.getElementById('test');

    var elemPosition = elem.getBoundingClientRect();
    var elemStyles = getComputedStyle(elem);
    var imgName = elem.firstElementChild.firstElementChild.getAttribute('src');
    test.style.left = elemPosition.left + 'px';
    test.style.top = elemPosition.top + pageYOffset + 'px';
    test.style.width = elemStyles.width;
    test.style.height = elemStyles.height;
    test.innerHTML = `<img src="${imgName}" alt="">`;
   
    var container = document.getElementsByClassName('container-fluid')[0];
    container.style.filter = 'blur(100px)'; 
    container.classList.add('clickability');
    test.style.left = '35%';
    test.style.width = '300px'
    }
}
  getImages();

}());

function liveCard() {
  document.onmousemove = function(e) {

    if (e.target.closest('.item__content')) {
      var elem = e.target.closest('.item__content');
      var computedStyle = getComputedStyle(elem);
      var coords = elem.getBoundingClientRect();
      var center = {
        y: coords.top + pageYOffset + parseInt(computedStyle.width) / 2,
        x: coords.left + pageXOffset + parseInt(computedStyle.height) / 2
      };
      var clientCoords = {
        x: e.pageX - center.x,
        y: center.y - e.pageY
      };
      elem.style.transform = "rotateY(" + Math.round(clientCoords.x / 20) + "deg) rotateX(" + Math.round(clientCoords.y / 20) + "deg)";
      // console.log( "rotateY(" + Math.round(clientCoords.x/20) + "deg) rotateX(" + Math.round(clientCoords.y/20) + "deg)" );
      e.target.onmouseleave = function() {
        elem.style.transform = "rotateY(0deg) rotateX(0deg)";
      }
    }
  }
}

liveCard();

///////////////////// MASONRY

function updateMasonry() {

  $(".masonry-container").imagesLoaded(function() {
    $(".masonry-container").masonry({
      columnWidth: ".item",
      itemSelector: ".item"
    })
  })

  ///////////////////// COVER ITEM

  $(document).ready(function() {
    $(".item").hover(
      function() {
        $(this).find(".cover-item-gallery").stop().fadeIn("slow");
      },
      function() {
        $(this).find(".cover-item-gallery").stop().fadeOut(0);
      })
  });
}

